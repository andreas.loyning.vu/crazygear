# Heroes
There are 3 Heroes in the game: **Warrior**, **Ranger** and **Mage**

## Warrior
-   Base: **150 HP**, **10 Str**, **3 Dex**, **1 Int**
-   Scale (Per Lvl): **30 HP**, **5 Str**, **2 Dex**, **1 Int** 

## Ranger
-   Base: **120 HP**, **5 Str**, **10 Dex**, **2 Int**
-   Scale (Per Lvl): **20 HP**, **2 Str**, **5 Dex**, **1 Int** 

## Warrior
-   Base: **100 HP**, **2 Str**, **3 Dex**, **10 Int**
-   Scale (Per Lvl): **15 HP**, **1 Str**, **2 Dex**, **5 Int** 


