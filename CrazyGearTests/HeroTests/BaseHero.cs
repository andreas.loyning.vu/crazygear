using System;
using CrazyGear.Hero;
using Xunit;

namespace CrazyGearTests.Hero
{
    public class BaseHeroTests
    {
        [Fact]
        public void Leveling_Fact(){
            Warrior test = new Warrior();
            test.GainXP(100);
            Assert.Equal(2, test.HeroStats.Level);

            test.GainXP(110);
            Assert.Equal(3, test.HeroStats.Level);

            test.GainXP(121);
            Assert.Equal(4, test.HeroStats.Level);

            test.GainXP(133);
            Assert.Equal(5, test.HeroStats.Level);

            test.GainXP(146);
            Assert.Equal(6, test.HeroStats.Level);

            test.GainXP(160);
            Assert.Equal(7, test.HeroStats.Level);

            Warrior test2 = new Warrior();
            test2.GainXP(770);
            Assert.Equal(7, test2.HeroStats.Level);
        }
    }
}