using System;
using CrazyGear.Enums;
using CrazyGear.Hero;
using CrazyGear.Item;
using CrazyGear.Struct;
using Xunit;

namespace CrazyGearTests.Hero
{
    public class WarriorTests
    {
        [Fact]
        public void WarriorStatat0_Fact()
        {
            Warrior test = new Warrior();
            Assert.Equal(150, test.HeroStats.HP);
            Assert.Equal(10, test.HeroStats.Strength);
            Assert.Equal(3, test.HeroStats.Dexterity);
            Assert.Equal(1, test.HeroStats.Intelligence);
            Assert.Equal(1, test.HeroStats.Level);
        }

        [Fact]
        public void WarriorLevelUp_Fact()
        {
            Warrior test = new Warrior();
            test.GainXP(100);

            Assert.Equal(180, test.HeroStats.HP);
            Assert.Equal(15, test.HeroStats.Strength);
            Assert.Equal(5, test.HeroStats.Dexterity);
            Assert.Equal(2, test.HeroStats.Intelligence);
            Assert.Equal(2, test.HeroStats.Level);
        }

        [Fact]
        public void WarriorStatatLv9_Fact()
        {
            Warrior test = new Warrior();
            test.GainXP(1200);
            Assert.Equal(390, test.HeroStats.HP);
            Assert.Equal(50, test.HeroStats.Strength);
            Assert.Equal(19, test.HeroStats.Dexterity);
            Assert.Equal(9, test.HeroStats.Intelligence);
            Assert.Equal(9, test.HeroStats.Level);
        }

        [Fact]
        public void WarriorAttack_Fact()
        {
            Warrior myCoolHero = new Warrior();
            myCoolHero.GainXP(1200);
            var myWeapon = new Weapon("Solomons Greatsword", new WeaponStats(5, WeaponType.MELEE));
            var myArMOR = new Armor("Dragon Platebody", new ArmorStats(5, ArmorType.PLATE, ItemSlot.BODY));
            var TotalDamage = (int)(25 + (50 * 1.5));

            myCoolHero.EquipItem(myWeapon);
            Assert.Equal($"You hit for {TotalDamage} damage!", myCoolHero.Attack());
        }
        [Fact]
        public void WarriorAttack_With_Armor_Fact()
        {
            Warrior myCoolHero = new Warrior();
            myCoolHero.GainXP(1200);
            var myWeapon = new Weapon("Solomons Greatsword", new WeaponStats(5, WeaponType.MELEE));
            var myArMOR = new Armor("Dragon Platebody", new ArmorStats(5, ArmorType.PLATE, ItemSlot.BODY));
            var TotalDamage = (int)(25 + ((50 + 13) * 1.5)); // 25 base from weapon, 50 from hero, and 13 from the platebody

            myCoolHero.EquipItem(myWeapon);
            myCoolHero.EquipItem(myArMOR);
            Assert.Equal($"You hit for {TotalDamage} damage!", myCoolHero.Attack());
        }
    }
}