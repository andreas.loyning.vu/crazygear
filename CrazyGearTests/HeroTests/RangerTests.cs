using System;
using CrazyGear.Hero;
using Xunit;

namespace CrazyGearTests.Hero
{
    public class HeroTests
    {
        [Fact]
        public void RangerStatat0_Fact()
        {
            Ranger test = new Ranger();
            Assert.Equal(120, test.HeroStats.HP);
            Assert.Equal(5, test.HeroStats.Strength);
            Assert.Equal(10, test.HeroStats.Dexterity);
            Assert.Equal(1, test.HeroStats.Intelligence);
            Assert.Equal(1, test.HeroStats.Level);
        }

        [Fact]
        public void RangerLevelUp_Fact()
        {
            Ranger test = new Ranger();
            test.GainXP(100);

            Assert.Equal(140, test.HeroStats.HP);
            Assert.Equal(7, test.HeroStats.Strength);
            Assert.Equal(15, test.HeroStats.Dexterity);
            Assert.Equal(2, test.HeroStats.Intelligence);
            Assert.Equal(2, test.HeroStats.Level);
        }
    }
}