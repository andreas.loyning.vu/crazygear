using System;
using CrazyGear.Hero;
using Xunit;

namespace CrazyGearTests.Hero
{
    public class MageTests
    {
        [Fact]
        public void MageStatat0_Fact()
        {
            Mage test = new Mage();
            Assert.Equal(100, test.HeroStats.HP);
            Assert.Equal(2, test.HeroStats.Strength);
            Assert.Equal(3, test.HeroStats.Dexterity);
            Assert.Equal(10, test.HeroStats.Intelligence);
            Assert.Equal(1, test.HeroStats.Level);
        }

        [Fact]
        public void MageLevelUp_Fact()
        {
            Mage test = new Mage();
            test.GainXP(100);
            Assert.Equal(115, test.HeroStats.HP);
            Assert.Equal(3, test.HeroStats.Strength);
            Assert.Equal(5, test.HeroStats.Dexterity);
            Assert.Equal(15, test.HeroStats.Intelligence);
            Assert.Equal(2, test.HeroStats.Level);
        }
    }
}