using System;
using Xunit;
using CrazyGear.Item;
using CrazyGear.Enums;

namespace CrazyGearTests.Item.Armors
{
    public class PlateArmorTests
    {
        [Fact]
        public void CreateBaseItem()
        {
            Armor plateBody = new Armor("test", new ArmorStats(15, ArmorType.PLATE, ItemSlot.BODY));
            Assert.Equal(210, plateBody.ArmorStats.BonusHP);
            Assert.Equal(33, plateBody.ArmorStats.BonusStr);
            Assert.Equal(16, plateBody.ArmorStats.BonusDex);
        }
    }
}