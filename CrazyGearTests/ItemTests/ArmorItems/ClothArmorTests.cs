using System;
using Xunit;
using CrazyGear.Item;
using CrazyGear.Enums;

namespace CrazyGearTests.Item.Armors
{
    public class ClothArmorTests
    {
        [Fact]
        public void ClothLegs()
        {
            Armor clothItem = new Armor("test", new ArmorStats(10, ArmorType.CLOTH, ItemSlot.LEGS));
            Assert.Equal(6, (int)(clothItem.ArmorStats.BonusDex * 0.6));
            Assert.Equal(13, (int)(clothItem.ArmorStats.BonusInt * 0.6));
            Assert.Equal(36, (int)(clothItem.ArmorStats.BonusHP * 0.6));
        }
    }
}