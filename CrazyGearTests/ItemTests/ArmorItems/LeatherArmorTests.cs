using System;
using CrazyGear.Enums;
using CrazyGear.Item;
using Xunit;

namespace CrazyGearTests.Item.Armors
{
    public class LeatherArmorTests
    {
        [Fact]
        public void CreateBaseItem()
        {
            Armor plateBody = new Armor("test", new ArmorStats(15, ArmorType.PLATE, ItemSlot.BODY));
            Assert.Equal(210, plateBody.ArmorStats.BonusHP);
            Assert.Equal(33, plateBody.ArmorStats.BonusStr);
            Assert.Equal(16, plateBody.ArmorStats.BonusDex);
        }
    }
}