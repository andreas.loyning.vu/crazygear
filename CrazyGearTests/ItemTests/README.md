# Armor
Armors can apply bonuses to Healt, Str, Dex and/or Int.
The slot effects how the armor stats are scaled after calculations.

## Cloth
*   Base stats: **10 HP**, **3 Int**, **1 Dex**
*   Scale: **5 HP**, **2 Int**, **1 Dex**

## Leather
*   Base stats: **20 HP**, **3 Dex**, **1 Str**
*   Scale: **8 HP**, **2 Dex**, **1 Str**

## Plate armor
*   Base stats: **30 HP**, **3 Str**, **1 Dex**
*   Scale: **12 HP**, **2 Str**, **1 Dex**

## Armor slot scaling
*   Body is **100%**
*   Head is **80%**
*   Legs are **60%**
