# Weapons
All weapons have a base damage which is increased by Dex for Ranged,
Str for Melee and Int for Magic.

Weapon have no extra stats on them.

## Melee weapons
*   Base damage: 15
*   scales +2 per level
### When attacking
Damage dealt is [**Weapon damage** + **Character effective strength** * 1.5] floored

## Ranged Weapons
*   Base damage: 5
*   Scales +3 per level
### When attacking
Damage dealt is [**Weapon damage** + **Character effective strength** * 2] floored

## Magic weapons
*   Base damage: 25
*   scales +2 per level

## When attacking
damage deal is [**Weapon damage** + **Character effective strength** * 3] floored
