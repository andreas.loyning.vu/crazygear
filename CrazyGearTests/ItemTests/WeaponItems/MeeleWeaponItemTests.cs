using System;
using Xunit;
using CrazyGear.Item;
using CrazyGear.Enums;
using CrazyGear.Struct;

namespace CrazyGearTests.Item.Weapons
{
    public class MeleeWeaponItemTests
    {
        [Fact]
        public void CreateMeleeItem_Fact()
        {
            Weapon a = new Weapon("test", new WeaponStats(5, WeaponType.MELEE));
            Assert.Equal(25, a.WeaponStats.Damage);
            Assert.Equal(5, a.WeaponStats.Level);
        }
    }
}