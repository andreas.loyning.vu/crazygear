using System;
using CrazyGear.Enums;
using CrazyGear.Item;
using CrazyGear.Struct;
using Xunit;

namespace CrazyGearTests.Item.Weapons
{
    public class RangedWeaponItemTests
    {
        [Fact]
        public void CreateRangedWeapon_Fact()
        {
            Weapon a = new Weapon("test", new WeaponStats(10, WeaponType.RANGED));
            Assert.Equal(35, a.WeaponStats.Damage);
            Assert.Equal(10, a.WeaponStats.Level);
        }
    }
}