using System;
using CrazyGear.Enums;
using CrazyGear.Item;
using CrazyGear.Struct;
using Xunit;

namespace CrazyGearTests.Item.Weapons
{
    public class MagicWeaponItemTests
    {
        [Fact]
        public void CreateMagicWeapon_Fact()
        {
            Weapon test = new Weapon("test", new WeaponStats(10, WeaponType.MAGIC));
            Assert.Equal(10, test.Level);
            Assert.Equal(45, test.WeaponStats.Damage);
        }
    }
}