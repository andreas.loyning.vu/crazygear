using CrazyGear.Hero;
using CrazyGear.Item;

namespace CrazyGear.Utils
{
    public static class DisplayExtensions
    {
        public static string Display(this Weapon weapon)
        {
            return $"Item stats for: {weapon.Name}\n" +
                   $"Weapon Type: {weapon.WeaponStats.WeaponType}\n" +
                   $"Weapon level: {weapon.WeaponStats.Level}\n" +
                   $"Damage: {weapon.WeaponStats.Damage}";
        }

       /*  public static string Display(this BaseHero hero)
        {
            return $"" +
                   $"" +
                   $"" +
                   $"";
        } */

        // TODO: Implement
        /* public static string Display(this Armor weapon)
        {
            return $"";
        } */
    }
}