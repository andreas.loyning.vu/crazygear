﻿using System;
using CrazyGear.Hero;
using CrazyGear.Item;
using CrazyGear.Struct;
using CrazyGear.Enums;

namespace CrazyGear
{
    class Program
    {
        static void Main(string[] args)
        {
            IHero myCoolHero = new Warrior();
            myCoolHero.GainXP(1200);
            Console.WriteLine(myCoolHero.Display() + "\n");

            // Weapons
            var godsword = new Weapon("Bandos Godsword", new WeaponStats(5, WeaponType.MELEE));
            var heavyBalista = new Weapon("Heavy balista", new WeaponStats(20, WeaponType.RANGED));
            var kodaiWand = new Weapon("Kodai wand", new WeaponStats(9, WeaponType.MAGIC));

            // Armor
            var dragonPlateBody = new Armor("Dragon Platebody", new ArmorStats(5, ArmorType.PLATE, ItemSlot.BODY));
            var plateLegs = new Armor("Dragon plateLegs", new ArmorStats(5, ArmorType.PLATE, ItemSlot.LEGS));
            var clothHead = new Armor("Studded leather coif", new ArmorStats(5, ArmorType.LEATHER, ItemSlot.HEAD));
            var mysticLegs = new Armor("Mystic robe bottom", new ArmorStats(5, ArmorType.CLOTH, ItemSlot.LEGS));

            Console.WriteLine(dragonPlateBody.ArmorStats.ToString() + "\n");
            System.Console.WriteLine(godsword.WeaponStats.ToString() + "\n");

            myCoolHero.EquipItem(godsword);
            // Equipping multiple armor does not work
            myCoolHero.EquipItem(dragonPlateBody);
            myCoolHero.EquipItem(dragonPlateBody);

            Console.WriteLine(myCoolHero.Display() + "\n");

            // With platearmor and weapon
            Console.WriteLine(myCoolHero.Attack() + "\n");

            myCoolHero.UnequipItem(dragonPlateBody);

            // Just weapon
            System.Console.WriteLine(myCoolHero.Attack() + "\n");

            myCoolHero.EquipItem(plateLegs);

            // With legs adn weapon
            System.Console.WriteLine(myCoolHero.Attack() + "\n");

            // equipping armor with higher level than the hero does not work
            myCoolHero.EquipItem(heavyBalista);
            System.Console.WriteLine(myCoolHero.Attack() + "\n");

            // Equiping magic weapon
            myCoolHero.EquipItem(kodaiWand);
            System.Console.WriteLine(myCoolHero.Attack() + "\n");
        }
    }
}
