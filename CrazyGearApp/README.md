# CrazyGear
An innovative console RPG Game! Select from 3 different Heroes, collect items and fight monster![^1]

### Heroes
-   Warrior
-   Ranger
-   Mage

### Weapon types
-   Melee
-   Ranged
-   Magic

### Armor types
-   Cloth
-   Leather
-   Plate

## Functionality
-   Create new heroes
-   Create different types of armor and weapons
-   Equip and unequip the hero with armor and weapons
-   Attack
-   Gain experience and level up

[^1]: Not actually true.
## Running the app
Run the command: `dotnet run`

## Building and running
To build the app, run the command `dotnet build`.
The executable file will be in the bin folder. Run the executable.


