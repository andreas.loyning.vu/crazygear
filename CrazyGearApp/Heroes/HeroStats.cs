namespace CrazyGear.Hero
{
    /// <summary>
    /// Datastructure which contains all relevant stats for the heroes
    /// </summary>
    public class HeroStats
    {
        public int Level { get; set; }
        public int HP { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Strength { get; set; }
        public int XP { get; set; }

        /// <summary>
        /// Creates a new instance of the HeroStats class
        /// </summary>
        /// <param name="stats">Stats</param>
        public HeroStats((int level, int hp, int dexterity, int intelligence, int strength, int xp) stats)
        {
            Level = stats.level;
            HP = stats.hp;
            Dexterity = stats.dexterity;
            Intelligence = stats.intelligence;
            Strength = stats.strength;
            XP = stats.xp;
        }

        public override string ToString()
        {
            return  $"HP: {HP}\n" +
                    $"Str: {Strength}\n" +
                    $"Dex: {Dexterity}\n" +
                    $"Int: {Intelligence}\n" +
                    $"Lvl: {Level}";
        }
    }
}