using CrazyGear.Item;
using CrazyGear.Struct;

namespace CrazyGear.Hero
{
    /// <summary>
    /// The common interface for all Heroes.
    /// Declares all functionality for the hero
    /// </summary>
    public interface IHero
    {
        /// <summary>
        /// Attacks
        /// </summary>
        /// <returns>String representing the action</returns>
        string Attack();

        /// <summary>
        /// Gains xp to the Hero
        /// </summary>
        /// <param name="xp">XP to be added to the Hero</param>
        void GainXP(int xp);
        /// <summary>
        /// Equips the Hero with an item
        /// </summary>
        /// <param name="item">The item to be equipped</param>
        /// <returns>True if the Hero successfully eqipped the item, false if not</returns>
        void EquipItem(BaseItem item);
        /// <summary>
        /// Unequps a selected item from the Hero
        /// </summary>
        /// <param name="item">The item to remove</param>
        void UnequipItem(BaseItem item);
        /// <summary>
        /// Display info about the Hero
        /// </summary>
        /// <returns>info</returns>
        string Display();
    }
}