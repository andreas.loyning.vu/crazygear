using System.Collections.Generic;
using System.Linq;
using CrazyGear.Consts;
using CrazyGear.Enums;
using CrazyGear.Item;
using CrazyGear.Struct;

namespace CrazyGear.Hero
{
    /// <summary>
    /// Ranger Hero, has a lot of dexterity!
    /// </summary>
    public class Ranger : BaseHero
    {

        public Ranger() : base(HeroBaseStats.RANGER_BASE_STATS)
        {

        }

        public override void levelUp()
        {
            var stats = HeroScalingStats.RANGER_SCALING_STATS;
            HeroStats.Level++;
            HeroStats.HP += stats.hp;
            HeroStats.Intelligence += stats.intelligence;
            HeroStats.Dexterity += stats.dexterity;
            HeroStats.Strength += stats.strength;
        }
    }
}