using System.Collections.Generic;
using CrazyGear.Item;
using System.Linq;
using CrazyGear.Consts;
using CrazyGear.Enums;
using System;

namespace CrazyGear.Hero
{
    /// <summary>
    /// Warrior Hero, has a lot of strength!
    /// </summary>
    public class Warrior : BaseHero
    {
        public Warrior() : base(HeroBaseStats.WARRIOR_BASE_STATS)
        {

        }

        public override void levelUp()
        {
            var stats = HeroScalingStats.WARRIOR_SCALING_STATS;
            HeroStats.Level++;
            HeroStats.HP += stats.hp;
            HeroStats.Intelligence += stats.intelligence;
            HeroStats.Dexterity += stats.dexterity;
            HeroStats.Strength += stats.strength;
        }
    }
}