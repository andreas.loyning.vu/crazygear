using System;
using System.Collections.Generic;
using System.Linq;
using CrazyGear.Enums;
using CrazyGear.Item;

namespace CrazyGear.Hero
{
    /// <summary>
    /// The base hero that every other hero inherits from.
    /// Contains all basic functionality common to all heroes
    /// </summary>
    public abstract class BaseHero : IHero
    {
        /// <summary>
        /// Contains the stats of the Hero. See
        ///  <see cref="HeroStats" />
        /// </summary>
        public HeroStats HeroStats { get; private set; }
        /// <summary>
        /// List containing all equiped items
        /// </summary>
        public IList<BaseItem> Items { get; private set; }
        /// <summary>
        /// The xp threshold to hit to level up
        /// </summary>
        public int XPToNext { get; set; }
        /// <summary>
        /// Contains all the extra stats from armors
        /// </summary>
        public HeroStats BonusStats { get; set; }

        /// <summary>
        /// Creates a new instance of the BaseHero class
        /// </summary>
        /// <param name="stats">All the stats the Hero has</param>
        public BaseHero((int level, int hp, int dexterity, int intelligence, int strength, int xp) stats)
        {
            HeroStats = new HeroStats(stats);
            Items = new List<BaseItem>();
            BonusStats = new HeroStats((0, 0, 0, 0, 0, 0));

            // set level based on Hero xp
            HeroStats.Level = CalculateLevel();
            XPToNext = XPToNextLevel(HeroStats.XP);
        }
        public void GainXP(int xp)
        {
            HeroStats.XP += xp;
            this.HeroStats.Level = CalculateLevel();
            XPToNext = XPToNextLevel(HeroStats.XP);
        }

        /// <summary>
        /// Calculated the level of the Hero based on the current XP
        /// </summary>
        /// <returns>Level based on XP</returns>
        private int CalculateLevel()
        {
            int level = 1;
            int toNext = 100;
            while (HeroStats.XP >= toNext)
            {
                toNext += XpToNextFormula(level);
                levelUp();
                level++;
            }

            return level;
        }

        /// <summary>
        /// Updated the Hero with new stats. Different Hero classes levels up differently
        /// </summary>
        public abstract void levelUp();

        public void EquipItem(BaseItem newItem)
        {
            var itemInSlot = Items.Where(item => item.ItemSlot == newItem.ItemSlot);
            if (newItem.Level > HeroStats.Level)
            {
                return;
            }

            if (itemInSlot.Count() > 0)
            {
                RemoveBonuses(itemInSlot.ToArray()[0]);
                Items.Remove(itemInSlot.ToArray()[0]);
            }
            AddBonuses(newItem);
            Items.Add(newItem);
        }

        public void UnequipItem(BaseItem itemToRemove)
        {
            
            var itemExists = Items.Where(item => item == itemToRemove);
            if (itemExists.Count() > 0)
            {
                RemoveBonuses(itemToRemove);
                Items.Remove(itemToRemove);
            }
        }

        /// <summary>
        /// Adds the bonuses from the item to the Hero
        /// </summary>
        /// <param name="item"> The item to add from </param>
        public void AddBonuses(BaseItem item)
        {
            if (item is Armor)
            {
                var castedItem = (Armor)item;
                switch (item.ItemSlot)
                {
                    case ItemSlot.BODY:
                        AddStats(castedItem, 1);
                        break;
                    case ItemSlot.LEGS:
                        AddStats(castedItem, 0.6);
                        break;
                    case ItemSlot.HEAD:
                        AddStats(castedItem, 0.8);
                        break;
                    default:
                        break;
                }
            }
        }
        
        /// <summary>
        /// removes the Item from the Hero
        /// </summary>
        /// <param name="item">Item to be removed</param>
        public void RemoveBonuses(BaseItem item)
        {
            if (item is Armor)
            {
                var castedItem = (Armor)item;
                switch (item.ItemSlot)
                {
                    case ItemSlot.BODY:
                        RemoveStats(castedItem, 1);
                        break;
                    case ItemSlot.LEGS:
                        RemoveStats(castedItem, 0.6);
                        break;
                    case ItemSlot.HEAD:
                        RemoveStats(castedItem, 0.8);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Calculates the bonuses bases on the <c>item</c> and adds them to the Hero
        /// </summary>
        /// <param name="item">The item where stats are added to Hero</param>
        /// <param name="percentage"></param>
        private void AddStats(Armor item, double percentage)
        {
            BonusStats.HP += (int)(item.ArmorStats.BonusHP * percentage);
            BonusStats.Dexterity += (int)(item.ArmorStats.BonusDex * percentage);
            BonusStats.Intelligence += (int)(item.ArmorStats.BonusInt * percentage);
            BonusStats.Strength += (int)(item.ArmorStats.BonusStr * percentage);
        }
        /// <summary>
        /// Calculates the bonuses bases on the <c>item</c> and removes them to from the Hero
        /// </summary>
        /// <param name="item">The item where stats are added to Hero</param>
        /// <param name="percentage"></param>
        private void RemoveStats(Armor item, double percentage)
        {
            BonusStats.HP -= (int)(item.ArmorStats.BonusHP * percentage);
            BonusStats.Dexterity -= (int)(item.ArmorStats.BonusDex * percentage);
            BonusStats.Intelligence -= (int)(item.ArmorStats.BonusInt * percentage);
            BonusStats.Strength -= (int)(item.ArmorStats.BonusStr * percentage);
        }

        /// <summary>
        /// Finds how much xp is required to level
        /// </summary>
        /// <param name="xp"></param>
        /// <returns>The XP required to level to the next level</returns>
        private int XPToNextLevel(int xp)
        {
            int level = 1;
            int toNext = 100;
            while (HeroStats.XP >= toNext)
            {
                toNext += XpToNextFormula(level);
                level++;
            }

            return toNext - HeroStats.XP;
        }

        ///<summary>
        /// Returns the XP needed for the next level based on the current <paramref name="level"/>
        ///</summary>
        ///<param name="level">the current level</param>
        ///<returns><c>XP</c> required for leveling from level to the next</returns>
        private int XpToNextFormula(int level)
        {
            return (int)(100 * Math.Pow(1.1, level - 1));
        }

        public string Attack()
        {
            return $"You hit for {calculateDamage()} damage!";
        }

        /// <summary>
        /// Calculated the amoun of damage delt based on character stats, armor and weapons
        /// </summary>
        /// <returns>Damage done</returns>
        private int calculateDamage()
        {
            IEnumerable<BaseItem> weapons = Items.Where(item => item.ItemSlot == ItemSlot.WEAPON);

            if (weapons.Count() == 0)
            {
                return 0;
            }

            Weapon weapon = (Weapon)weapons.ToArray()[0];

            int weaponDamage = weapon.WeaponStats.Damage;
            WeaponType type = weapon.WeaponStats.WeaponType;

            switch (type)
            {
                case WeaponType.MELEE:
                    return (int)(weaponDamage + ((BonusStats.Strength     +  HeroStats.Strength)       *   1.5));
                case WeaponType.RANGED:
                    return (int)(weaponDamage + ((BonusStats.Dexterity    +  HeroStats.Dexterity)      *   2));
                case WeaponType.MAGIC:
                    return (int)(weaponDamage + ((BonusStats.Intelligence +  HeroStats.Intelligence)   *   3));
                default:
                    return 0;
            }
        }

        public string Display()
        {
            return HeroStats.ToString();
        }
    }
}