using CrazyGear.Consts;
using CrazyGear.Enums;
using CrazyGear.Item;
using CrazyGear.Struct;
using System.Collections.Generic;
using System.Linq;

namespace CrazyGear.Hero
{
    /// <summary>
    /// Mage Hero, has high intelligence!
    /// </summary>
    public class Mage : BaseHero
    {

        public Mage() : base(HeroBaseStats.MAGIC_BASE_STATS)
        {

        }

        public override void levelUp()
        {
            var stats = HeroScalingStats.MAGIC_SCALING_STATS;
            HeroStats.Level++;
            HeroStats.HP += stats.hp;
            HeroStats.Intelligence += stats.intelligence;
            HeroStats.Dexterity += stats.dexterity;
            HeroStats.Strength += stats.strength;
        }
    }
}