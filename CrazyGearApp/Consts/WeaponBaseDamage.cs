using CrazyGear.Struct;

namespace CrazyGear.Consts
{
    /// <summary>
    /// consatants for the base damage for different weapon types
    /// </summary>
    public static class WeaponBaseDamage
    {
        public static int MELEE = 15;
        public static int RANGED = 5;
        public static int MAGIC = 25;
    }
}