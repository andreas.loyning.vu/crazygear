using CrazyGear.Struct;

namespace CrazyGear.Consts
{
    /// <summary>
    /// Constants for the base stats for different heroes
    /// </summary>
    public static class HeroBaseStats
    {
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) WARRIOR_BASE_STATS = (1, 150, 3, 1, 10, 0);
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) RANGER_BASE_STATS = (1, 120, 10, 1, 5, 0);
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) MAGIC_BASE_STATS = (1, 100, 3, 10, 2, 0);
    }
}