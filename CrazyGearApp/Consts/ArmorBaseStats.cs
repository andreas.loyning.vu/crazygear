namespace CrazyGear.Consts
{
    /// <summary>
    /// Constants for the base stats for different armors
    /// </summary>
    public static class ArmorBaseStats
    {
        public static (int bonusHP, int bonusInt, int bonusDex) CLOTH = (bonusHP: 10, bonusInt: 3, bonusDex: 1);
        public static (int bonusHP, int bonusDex, int bonusStr) LEATHER = (bonusHP: 20, bonusDex: 3, bonusStr: 1);
        public static (int bonusHP, int bonusStr, int bonusDex) PLATE = (bonusHP: 30, bonusStr: 3, bonusDex: 1);
    }
}