using CrazyGear.Enums;
using CrazyGear.Item;

namespace CrazyGear.Consts
{
    /// <summary>
    /// Constants used for scaling armor
    /// </summary>
    public static class ArmorScalingStats
    {
        public static (int bonusHP, int bonusInt, int bonusDex) CLOTH = (bonusHP: 5, bonusInt: 2, bonusDex: 1);
        public static (int bonusHP, int bonusDex, int bonusStr) LEATHER = (bonusHP: 8, bonusDex: 2, bonusStr: 1);
        public static (int bonusHP, int bonusStr, int bonusDex) PLATE = (bonusHP: 12, bonusStr: 2, bonusDex: 1);

    }
}