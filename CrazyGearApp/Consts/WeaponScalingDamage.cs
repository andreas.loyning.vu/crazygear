using CrazyGear.Struct;

namespace CrazyGear.Consts
{
    /// <summary>
    /// Constants for the scaling values for different weapon types
    /// </summary>
    public static class WeaponScalingStats
    {
        public static int MELEE = 2;
        public static int RANGED = 3;
        public static int MAGIC = 2;
    }
}