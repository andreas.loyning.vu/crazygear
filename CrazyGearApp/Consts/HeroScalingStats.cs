namespace CrazyGear.Consts
{
    /// <summary>
    /// Constants for the scaling values for different heroes
    /// </summary>
    public static class HeroScalingStats
    {
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) WARRIOR_SCALING_STATS = (1, 30, 2, 1, 5, 0);
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) RANGER_SCALING_STATS = (1, 20, 5, 1, 2 , 0);
        public static (int level, int hp, int dexterity, int intelligence, int strength, int xp) MAGIC_SCALING_STATS = (1, 15, 2, 5, 1, 0);
    }
}