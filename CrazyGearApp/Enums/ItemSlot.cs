namespace CrazyGear.Enums
{
    /// <summary>
    /// Enums for the different item slots
    /// </summary>
    public enum ItemSlot
    {
        BODY,
        HEAD,
        LEGS,
        WEAPON,
    }
}