namespace CrazyGear.Enums
{
    /// <summary>
    /// Enums for the different weapon types
    /// </summary>
    public enum WeaponType
    {
        MELEE,
        RANGED,
        MAGIC,
    }
}