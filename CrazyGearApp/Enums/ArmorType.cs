namespace CrazyGear.Enums
{
    /// <summary>
    /// Enums for the different armor types
    /// </summary>
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        PLATE,
    }
}