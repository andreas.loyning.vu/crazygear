using CrazyGear.Enums;
using CrazyGear.Consts;

namespace CrazyGear.Item
{
    /// <summary>
    /// Data strcuture which contains all relevant stats for an armor piece
    /// </summary>
    public struct ArmorStats
    {
        public int BonusHP { get; }
        public int BonusDex { get; }
        public int BonusInt { get; }
        public int BonusStr { get; }
        public int Level { get; }
        public ArmorType ArmorType { get; }
        public ItemSlot ItemSlot { get; }

        /// <summary>
        /// Sets the values of the bonus stats based on the <see cref="ArmorType"/> and the <c>level</c>
        /// </summary>
        /// <param name="level">Item level</param>
        /// <param name="armorType">see <see cref="ArmorType"/></param>
        /// <param name="itemSlot">see <see cref="ItemSlot"/></param>
        public ArmorStats(int level, ArmorType armorType, ItemSlot itemSlot)
        {
            Level = level;
            ArmorType = armorType;
            ItemSlot = itemSlot;
            switch (armorType)
            {
                case ArmorType.LEATHER:
                    var leatherStats = ArmorBaseStats.LEATHER;
                    var leatherScaleStats = ArmorScalingStats.LEATHER;
                    BonusHP = leatherStats.bonusHP + (level * leatherScaleStats.bonusHP);
                    BonusDex = leatherStats.bonusDex + (level * leatherScaleStats.bonusDex);
                    BonusStr = leatherStats.bonusStr + (level * leatherScaleStats.bonusStr);
                    BonusInt = 0;
                    break;
                case ArmorType.CLOTH:
                    var clothStats = ArmorBaseStats.CLOTH;
                    var clothScaleStats = ArmorScalingStats.CLOTH;
                    BonusHP = clothStats.bonusHP + (level * clothScaleStats.bonusHP);
                    BonusDex = clothStats.bonusDex + (level * clothScaleStats.bonusDex);;
                    BonusInt = clothStats.bonusInt + (level * clothScaleStats.bonusInt);;
                    BonusStr = 0;
                    break;
                case ArmorType.PLATE:
                    var plateStats = ArmorBaseStats.PLATE;
                    var plateScaleStats = ArmorScalingStats.PLATE;
                    BonusHP = plateStats.bonusHP + (level * plateScaleStats.bonusHP);
                    BonusStr = plateStats.bonusStr + (level * plateScaleStats.bonusStr);;
                    BonusDex = plateStats.bonusDex + (level * plateScaleStats.bonusDex);;
                    BonusInt = 0;
                    break;
                default:
                    BonusHP = 0;
                    BonusStr = 0;
                    BonusDex = 0;
                    BonusInt = 0;
                    break;
            }
        }

        /// <summary>
        /// Returns formattet values of the class
        /// </summary>
        /// <returns>Formattet stats</returns>
        public override string ToString()
        {
            return  $"Armor Type: {ArmorType}\n" +
                    $"Slot: {ItemSlot}\n" +
                    $"Level: {Level}\n" +
                    $"Bonus HP: {BonusHP}\n" +
                    $"{(BonusDex > 0 ? $"Dex: {BonusDex}\n" : "")}" +
                    $"{(BonusInt > 0 ? $"Int: {BonusInt}\n" : "")}" +
                    $"{(BonusStr > 0 ? $"Str: {BonusStr}" : "")}";
        }
    }
}