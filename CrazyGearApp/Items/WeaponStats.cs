using CrazyGear.Enums;
using CrazyGear.Consts;

namespace CrazyGear.Struct
{
    /// <summary>
    /// Data strcuture which contains all relevant stats for a weapon
    /// </summary>
    public struct WeaponStats
    {
        public int Damage { get; }
        public int Level { get; }
        public WeaponType WeaponType { get; }

        /// <summary>
        /// Sets the damage value based on the <see cref="WeponType"/> and <c>level<c/>
        /// </summary>
        /// <param name="level"></param>
        /// <param name="weaponType"></param>
        public WeaponStats(int level, WeaponType weaponType)
        {
            switch (weaponType)
            {
                case WeaponType.MELEE:
                    Damage = WeaponBaseDamage.MELEE + (level * WeaponScalingStats.MELEE);
                    break;
                case WeaponType.RANGED:
                    Damage = WeaponBaseDamage.RANGED + (level * WeaponScalingStats.RANGED);
                    break;
                case WeaponType.MAGIC:
                    Damage = WeaponBaseDamage.MAGIC + (level * WeaponScalingStats.MAGIC);
                    break;
                default:
                    Damage = -1;
                    break;
            }
            Level = level;
            WeaponType = weaponType;
        }

        /// <summary>
        /// Returns formattet values of the class
        /// </summary>
        /// <returns>Formattet stats</returns>
        public override string ToString()
        {
            return  $"Weapon Type: {WeaponType}\n" +
                    $"Weapon level: {Level}\n" +
                    $"Damage: {Damage}";
        }
    }
}