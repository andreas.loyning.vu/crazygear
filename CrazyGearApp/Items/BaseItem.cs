using CrazyGear.Enums;

namespace CrazyGear.Item
{
    /// <summary>
    /// The base item of all items.
    /// Contains all common properties
    /// </summary>
    public abstract class BaseItem
    {
        public string Name { get; set; }
        public ItemSlot ItemSlot { get; set; }
        public int Level { get; set; }

        public BaseItem(string name, ItemSlot itemSlot, int level)
        {
            Name = name;
            ItemSlot = itemSlot;
            Level = level;
        }
    }
}