using CrazyGear.Enums;

namespace CrazyGear.Item
{
    /// <summary>
    /// An armor which contains the relevant <see cref="ArmorStats"/>
    /// </summary>
    public class Armor : BaseItem
    {
        public ArmorStats ArmorStats { get; set; }
        public Armor(string name, ArmorStats armorStats) : base(name, armorStats.ItemSlot, armorStats.Level)
        {
            ArmorStats = armorStats;
        }
    }
}