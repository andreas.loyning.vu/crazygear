using CrazyGear.Struct;
using CrazyGear.Enums;

namespace CrazyGear.Item
{
    /// <summary>
    /// A weapon which contains all relevant <see cref="WeaponStats"/>
    /// 
    /// </summary>
    public class Weapon : BaseItem
    {
        public WeaponStats WeaponStats { get; }
        public Weapon(string name, WeaponStats weaponStats) : base(name, ItemSlot.WEAPON, weaponStats.Level)
        {
            Name = name;
            WeaponStats = weaponStats;
        }
    }
}